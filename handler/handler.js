//Handler for the commands


//Load TCP library
net = require('net');

exports.resolve = function(args, client, clientHashtable, channelHashtable)
{
	switch(args[0])
	{
		case "/nick":
			resolveNick(args, client, clientHashtable, channelHashtable);
			break;

		case "/quit":
			client.socket.end();
			clientHashtable.remove(client.nick);
			break;
		default:
			client.socket.write("[SYSTEM]: Command not recognized.");
			break;
	}
}

//Send message to all but sender
function broadcast(message, sender, hashtable)
{
	hashtable.forEach(function(key)
	{
		var client = hashtable.get(key);
		if(client.socket === sender) return;
		client.socket.write(message);
	});
}

//Handle nick command
function resolveNick(args, client, clientHashtable, channelHashtable)
{
	if(args[1])
	{
		//Check if client has nick, if it doesn't it means it's a first connection
		if(!client.nick)
		{
			//Check if table has intended nick
			if(!clientHashtable.has(args[1]))
			{
				//Assign nickname
				client.nick = args[1];
				//Store in hashtable
				clientHashtable.put(args[1], client);
				//Inform chat of changes
				client.socket.write("Welcome to the server " + client.nick + 
							"\n> You're currently at our main channel: The Hall\n> Lets keep it clean!");
				client.socket.write("Remember only the use of a single channel at a time is allowed.\n> Use the JOIN command to switch between channels.");
				broadcast(client.nick + " has connected to the server.", client.socket, clientHashtable);
				//Vinculate client with main channel
				var aux_obj = channelHashtable.get('Hall');
				aux_obj.members.push(args[1]);

			}
			else
			{
				client.socket.write("Nick already in use, please select another one.");
				client.socket.end();
			}
		}
		//The client has a nickname
		else
		{
			//Check the table for it
			if(!clientHashtable.has(args[1]))
			{
				//Put new entry in hashtable and Remove old one
				clientHashtable.put(args[1], client);
				clientHashtable.remove(client.nick);
				
				//Inform chat of changes
				broadcast(client.nick + " has changed nickname to " + args[1] + ".", client.socket, clientHashtable);

				//Assign new nickname
				client.nick = args[1];
			}
			//Check if intended nickname is not already client's nickname
			else if(args[1] !== client.nick)
				client.socket.write("Nick already in use, please select another one.");
			else
				client.socket.write("That's already your nick.");
		}
	}
	//No arguments, hence, wrong USAGE
	else
		client.socket.write("Too few arguments, please provide nickname.");
}
