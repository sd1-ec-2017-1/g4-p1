//Load TCP library
net = require('net');

//Load command handler
handler = require('../handler/handler.js');

//Load Hashtable library
Hashtable = require('hashtable');

//Create new Clients Hashtable
clientHashtable = new Hashtable();

//Create new Channels Hashtable
channelHashtable = new Hashtable();

//Load client entity
clientEntity = require('../entity/clientEntity.js');

//Load channel entity
channelEntity = require('../entity/channelEntity.js');

//Keep track of clients
//var clients = [];

//Insert main channel
channelHashtable.put('Hall', channelEntity.constructor('Hall'));



//Start TCP Server
net.createServer(function (socket)
{
	//Create client object and assign socket and attributes
	var curr_Client = clientEntity.constructor(socket);

	//Push new client into the list
	//clients.push(curr_Client);

	//Send Welcome message
	//socket.write("Welcome " + socket.name + "\n");
	//broadcast(socket.name + " joined the chat\n", socket);

	//Handle incoming data
	socket.on('data', function(data)
	{
		var args = data.toString().trim().split(" ");
		if(args[0][0] === '/')
			handler.resolve(args, curr_Client, clientHashtable, channelHashtable);
		else
		{
			console.log(data.toString().trim());
			broadcast(data.toString().trim(), curr_Client);
		}
	});

	//Remove client when it leaves
	socket.on('end', function()
	{
		if(curr_Client.nick !== null)
		{
			clientHashtable.remove(curr_Client.nick);
			inform(curr_Client.nick + " has disconnected.");
		}
	});

	function inform(message)
	{
		clientHashtable.forEach(function(key)
		{
			var client = clientHashtable.get(key);
			client.socket.write(message);
		});
	}
	function broadcast(message, sender)
	{
		clientHashtable.forEach(function(key)
		{
			//Don't send to the sender
			var client = clientHashtable.get(key);
			if(client.socket === sender.socket) return;
			client.socket.write("[" + sender.nick + "] " +
						message);
		});
	}


}).listen(6667);
