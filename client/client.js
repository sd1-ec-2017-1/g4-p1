//First based of: https://gist.github.com/tedmiston/5935757

//Load TCP library
net = require('net');

//Load readline library
const readline = require('readline');

//Create readline interface for dealing with user i/o
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

//Create client's socket
var client = new net.Socket();

//Wait for and deal with user input
rl.on('line', function(input) {
	tmp = input.toString().trim();
  	client.write(tmp);
  	process.stdout.write("> ");
});

//Check if correct USAGE
if(!process.argv[2])
{
	console.log("[USAGE]: node <client> [nick]");
	process.exit(1);
}

//Connect to localhost server
client.connect(6667, '127.0.0.1');

client.on('connect', function()
{
	console.log("> Connected");
	client.write("/nick " + process.argv[2]);
	process.stdout.write("> ");
});

client.on('data', function(data)
{
	if(data !== '')
		console.log(data.toString().trim());
	process.stdout.write("> ");
});

client.on('end', function()
{
	console.log("Connection closed.");
	process.exit(1);
});