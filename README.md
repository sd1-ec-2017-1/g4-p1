# Projeto 1 da disciplina de Sistemas Distribuídos 1 do curso de Eng. Computação da UFG

USAGE
===================
> É necessário instalar a biblioteca **Hashtable** (npm install hashtable). Para mais informações sobre a biblioteca **https://www.npmjs.com/package/hashtable**.
> - **nodejs server.js &** (para rodar em background);
> - **nodejs client.js [nick]**;

INFO
===================
> Para fazer uso da biblioteca Hashtable, é preciso, além da instalação da própria, adaptar o ambiente da VM fornecida pelo professor:
> - Em **https://nodejs.org** baixe o v7.7.4 Current;
> - Em seguida vá até o diretório em que se encontra o arquivo ".tar.xz" baixado e o extraia com o comando:
> - **tar -xvf fileName.tar.xz**
> - Entre na pasta bin/ do diretório criado, onde estarão os arquivos **node** (binário) e **npm** (link simbólico);
> - Agora, vá até o diretório **/usr/bin/** do sistema e execute os seguintes passos
> - **cd /usr/bin/**
> - Renomeie os binários das versões já instaladas do node e npm:
> - **mv node node_old**
> - **mv npm npm_old**
> - Agora crie os links simbólicos apontando para as versões latest do node e npm extraídas anteriormente:
> - **sudo ln -s /caminho/até/o/arquivo/node**
> - **sudo ln -s /caminho/até/o/arquivo/npm**
> - Execute os comandos **node -v** e **npm -v** para verificar a versão (Esperado v7.7.4 e v4.1.2 respectivamente);
> - Por fim, vá ao diretório raiz do projeto1 e instale a biblioteca:
> - **npm install hashtable**

FILES
===================
> - **/server/server.js**
> - **/client/client.js**
> - **/handler/handler.js**
> - **/entity/clientEntity.js**
> - **/node_modules/**
> - **README.md**

TO DO
===================
> - Comandos da seção 3.1, primeiramente;
> - Array[**char** mode] no clientEntity.js;
> - Usuário moderador inicial;
> - Hash para o password;
> - Salvar tabela hash em arquivo para persistência (para usuário registrados);
> - LER TODO O RFC E MEXER NO IRC.
